import React from 'react';
import ReactDOM from 'react-dom';
import ControllerPanel from "./ControllerPanel";
import PlayPanel from "./PlayPanel";
import '../css/App.css'

class App extends React.Component{

    constructor(props) {
        super(props);
         this.state = {
             correctAnswer: ''
         }
         this.handleRecive = this.handleRecive.bind(this);
    }

    handleRecive(value) {
        this.setState({
            correctAnswer: value
        }, ()=>console.log('App--corrotAnswer', value));
    }


    render() {
        return (
            <main className={'container'}>
                <PlayPanel />
                <ControllerPanel onChange={this.handleRecive} correctAnswer={this.state.correctAnswer} />
            </main>
        );
    }

}

export default App;
