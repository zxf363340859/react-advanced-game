import React from 'react';
import ReactDOM from 'react-dom';
import GuessPanel from "./GuessPanel";
import '../css/PlayPanel.css'

class PlayPanel extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            guessAnswer: ['', '', '', ''],
            success: false
        }

        this.handleGuess = this.handleGuess.bind(this);
    }

    handleGuess(value) {
        const tmp = this.state.guessAnswer = Array.of(value);
        this.setState({
            guessAnswer: tmp
        }, console.log('PlayPanel====guessAbswer', tmp));

        const flag = this.props.correctAnswer === String(this.state.guessAnswer);
        this.setState({
            success: flag
        }, console.log('PlayPanel====success', flag));
    }


    render() {
        return (
            <div className={'play-container'}>
                <h1>Your Result</h1>
                <div className={'play-input'}>
                    <input value={'A'}/>
                    <input value={'B'}/>
                    <input value={'C'}/>
                    <input value={'D'}/>
                </div>
                {
                    this.state.success ?
                        <p>success</p> :
                        <p>failed</p>
                }
                <GuessPanel onChange={this.handleGuess}/>
            </div>
        );
    }
}

export default PlayPanel;
