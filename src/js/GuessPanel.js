import React from 'react';
import ReactDOM from 'react-dom';
import '../css/GuessPanel.css'

class GuessPanel extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            guessAnswer: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(event) {
        const tmp = event.target.value
        this.setState({
            guessAnswer: tmp
        }, ()=>  console.log('guess change', tmp));
    }

    handleClick(event) {
        this.props.onChange(this.state.guessAnswer);
    }

    render() {
        return (
            <div className={'guess-container'}>
                <h1>Guess Card</h1>
                <div className={'guess-input'}>
                    <input type={'text'} onChange={this.handleChange}/>
                </div>
                <button className={'guess'} onClick={this.handleClick} >Guess</button>
            </div>
        );
    }

}

export default GuessPanel;
