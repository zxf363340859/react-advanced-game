import React from 'react';
import ReactDOM from 'react-dom';
import '../css/ControllerPanel.css'


class ControllerPanel extends React.Component{


    constructor(pros){
        super(pros);
        this.state = {
            chars: '    '
        }
        this.handleNewcard = this.handleNewcard.bind(this);
    }

    handleNewcard() {
        const arr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        let chars = '';
        for (let i = 4; i > 0; i--) {
            let tmp = Math.floor(Math.random() * (26 - 0) ) + 0;
            chars += arr[tmp];
        }
        this.setState({
            chars: chars
        }, (chars)=>console.log('chars=====', this.state.chars));
        console.log('tmp====', chars);

        setTimeout(()=> {
            this.setState({
                chars: '    '
            });
        }, 3000);

        this.props.onChange(chars);
    }



    render() {
        return (
            <div className={'controller-container'}>
                <h2 className={'new-card-font'}>New Card</h2>
                <button className={'new-card-button'} onClick={this.handleNewcard}>New Card</button> <br />
                <input value={this.state.chars[0]}/>
                <input value={this.state.chars[1]}/>
                <input value={this.state.chars[2]}/>
                <input value={this.state.chars[3]}/>
                <br />
                <button className={'show-result'}>Show Result</button>
            </div>
        );
    }


}

export default ControllerPanel;
